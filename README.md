# Pâques

Dates de Pâques pour le siècle courant.

## Compilation

```bash
lualatex siecle
```

Cette commande génère le fichier [siecle.pdf](./siecle.pdf).

## Algorithme

Les dates de Pâques sont calculées au moyen de l'algorithme Oudin, implémenté dans un script Lua.

```lua
-- Date de Pâques par l'algorithme Oudin

function EasterDay(AYear)
  local G = AYear % 19
  local C = AYear // 100
  local C4 = C // 4
  local E = (8 * C + 13) // 25
  local H = (19 * G + C - C4 - E + 15) % 30
  local K = H // 28
  local P = 29 // (H + 1)
  local Q = (21 - G) // 11
  local I = (K * P * Q - 1) * K + H
  local B = AYear + AYear // 4
  local J1 = B + I + 2 + C4 - C
  local J2 = J1 % 7
  return 28 + I - J2
end
``` 

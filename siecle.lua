
-- Date de Pâques par l'algorithme Oudin

function EasterDay(AYear)
  local G = AYear % 19
  local C = AYear // 100
  local C4 = C // 4
  local E = (8 * C + 13) // 25
  local H = (19 * G + C - C4 - E + 15) % 30
  local K = H // 28
  local P = 29 // (H + 1)
  local Q = (21 - G) // 11
  local I = (K * P * Q - 1) * K + H
  local B = AYear + AYear // 4
  local J1 = B + I + 2 + C4 - C
  local J2 = J1 % 7
  return 28 + I - J2
end

-- Siècle courant (en comptant à partir de zéro)

function CurrentCentury()
  return os.date('*t').year // 100
end

function PrintEasterDates()
  local LCentury = 100 * CurrentCentury()
  for LDecade = LCentury, LCentury + 90, 10 do
    
    tex.sprint(string.format('%d & ', LDecade))
    
    for LYear = LDecade, LDecade + 9 do
      local LMonth = 3
      local LDay = EasterDay(LYear)
      if LDay > 31 then
        LDay = LDay - 31
        LMonth = 4
      end
      
      local LDateStr =
        (LDay == 1 and '1\\ier' or tostring(LDay)) ..
        (LMonth == 3 and ' mars' or ' avr.') ..
        (LYear < LDecade + 9 and ' & ' or ' ')
      
      tex.sprint(LDateStr)
    end
    
    tex.sprint('\\\\')
  end
end

function PrintCentury()
  tex.sprint(string.format('%d', CurrentCentury() + 1))
end

if tex == nil then
  tex = {}
  tex.sprint = print
  PrintEasterDates()
end

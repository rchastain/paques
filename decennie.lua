
-- Fêtes catholiques dont la date dépend de celle de Pâques, pour l'année courante.

-- Jours juliens.
-- Conversion d'une date du calendrier grégorien en jour julien et d'un jour julien en date du
-- calendrier grégorien. Calcul d'une date à partir d'une autre date et d'une différence de jours.
-- Les algorithmes proviennent de la FAQ calendrier de Claus Tøndering :
-- http://www.tondering.dk/claus/cal/julperiod.php

function DateToJulianDayNumber(aYear, aMonth, aDay)
  local a = (14 - aMonth) // 12
  local b = aYear + 4800 - a
  local c = aMonth + 12 * a - 3
  
  return aDay + ((153 * c + 2) // 5) + 365 * b + (b // 4) - (b // 100) + (b // 400) - 32045
end

function JulianDayNumberToDate(aJulianDayNumber)
  local a = aJulianDayNumber + 32044
  local b = (4 * a + 3) // 146097
  local c = a - (146097 * b) // 4
  local d = (4 * c + 3) // 1461
  local e = c - ((1461 * d) // 4)
  local f = (5 * e + 2) // 153
  
  local lDay = e - ((153 * f + 2) // 5) + 1
  local lMonth = f + 3 - 12 * (f // 10)
  local lYear = 100 * b + d - 4800 + f // 10
  
  return lYear, lMonth, lDay
end

function RelativeDate(aYear, aMonth, aDay, aDiff)
  local lJDN = DateToJulianDayNumber(aYear, aMonth, aDay) + aDiff
  local lYear, lMonth, lDay = JulianDayNumberToDate(lJDN)
  return lYear, lMonth, lDay
end

-- Date de Pâques.
-- Date du dimanche de Pâques pour une année quelconque du calendrier
-- grégorien, par l'algorithme Oudin.

function Oudin(aYear)
  local G = aYear % 19
  local C = aYear // 100
  local C4 = C // 4
  local E = (8 * C + 13) // 25
  local H = (19 * G + C - C4 - E + 15) % 30
  local K = H // 28
  local P = 29 // (H + 1)
  local Q = (21 - G) // 11
  local I = (K * P * Q - 1) * K + H
  local B = aYear + aYear // 4
  local J1 = B + I + 2 + C4 - C
  local J2 = J1 % 7
  return 28 + I - J2
end

function EasterDate(aYear)
  local lMonth = 3
  local lDay = Oudin(aYear)

  if lDay > 31 then
    lDay = lDay - 31
    lMonth = 4
  end
  
  return lMonth, lDay
end

function FormatRelativeDate(aYear, aMonth, aDay, aDiff)
  local lMonthName = {'janv.', 'févr.', 'mars', 'avr.', 'mai', 'juin', 'juill.', 'août', 'sept.', 'oct.', 'nov.', 'déc.'}
  local lYear, lMonth, lDay = RelativeDate(aYear, aMonth, aDay, aDiff)
  local lResult = string.format('%d%s %s', lDay, lDay == 1 and '\\ier' or '', lMonthName[lMonth])
  return lResult
end

function CurrentYear()
  return os.date('*t').year
end

function FirstYearOfCurrentDecade()
  return (CurrentYear() // 10) * 10 
end

local dates = {
  Septuagesime = -63,
  Cendres = -46,
  Rameaux = -7,
  Paques = 0,
  Ascension = 39,
  Pentecote = 49,
  Trinite = 56,
  FeteDieu = 60
}

function FillTable(aYear)
  --~ aYear = 2020
  local lLines = {'', '', '', '', '', '', '', ''}
  for lYear = aYear, aYear + 9 do
    local lMonth, lDay = EasterDate(lYear)
    lLines[1] = lLines[1] .. ' & ' .. FormatRelativeDate(lYear, lMonth, lDay, dates.Septuagesime)
    lLines[2] = lLines[2] .. ' & ' .. FormatRelativeDate(lYear, lMonth, lDay, dates.Cendres)
    lLines[3] = lLines[3] .. ' & ' .. FormatRelativeDate(lYear, lMonth, lDay, dates.Rameaux)
    lLines[4] = lLines[4] .. ' & ' .. FormatRelativeDate(lYear, lMonth, lDay, dates.Paques)
    lLines[5] = lLines[5] .. ' & ' .. FormatRelativeDate(lYear, lMonth, lDay, dates.Ascension)
    lLines[6] = lLines[6] .. ' & ' .. FormatRelativeDate(lYear, lMonth, lDay, dates.Pentecote)
    lLines[7] = lLines[7] .. ' & ' .. FormatRelativeDate(lYear, lMonth, lDay, dates.Trinite)
    lLines[8] = lLines[8] .. ' & ' .. FormatRelativeDate(lYear, lMonth, lDay, dates.FeteDieu)
  end
  tex.sprint('\\toprule')
  tex.sprint(string.format('{} & %d & %d & %d & %d & %d & %d & %d & %d & %d & %d \\\\', aYear, aYear + 1, aYear + 2, aYear + 3, aYear + 4, aYear + 5, aYear + 6, aYear + 7, aYear + 8, aYear + 9))
  tex.sprint('\\midrule')
  tex.sprint(string.format('Septuagésime %s \\\\', lLines[1]))
  tex.sprint(string.format('Cendres %s \\\\', lLines[2]))
  tex.sprint(string.format('Rameaux %s \\\\', lLines[3]))
  tex.sprint(string.format('Pâques %s \\\\', lLines[4]))
  tex.sprint(string.format('Ascension %s \\\\', lLines[5]))
  tex.sprint(string.format('Pentecôte %s \\\\', lLines[6]))
  tex.sprint(string.format('Trinité %s \\\\', lLines[7]))
  tex.sprint(string.format('Fête-Dieu %s \\\\', lLines[8]))
  tex.sprint('\\bottomrule')
end

if tex == nil then
  tex = {}
  tex.sprint = print
  FillTable()
end
